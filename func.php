<?php
function arrToCsv($ar) { // declare function with input var $ar 
try { // Error handling
$ostr = ""; // Create the output string (blank, we need to append later)
foreach ($ar as &$fe) { // for each in the array, output item as $fe
    $ostr .= $fe.','; // Output string append ,item
}//loop
unset($fe); // dispose of $fe, we don't need it
return $ostr; // return string
}
catch(Exception $e) { // in case of error
  throw new Exception("Could not convert array to CSV. ". $e->getMessage()); // Throw exception with custom message
}
}
// Example use: $toadd[$i] = "\n" . arrToCsv($phone, $number);
?>