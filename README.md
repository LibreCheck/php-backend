# LibreCheck PHP Backend
The PHP file manager system for LibreCheck.

```
git clone https://gitlab.com/librecheck/php-backend
cd php-backend
php -S 127.0.0.1:8100
```

Make a `.librecheck` directory in your home folder, then put the following files there.

Include this basic file with random info as a `people.csv` file in the `~/.librecheck/config/` directory:

```
groupid,personid,first,last,guardname,dob,gender,medical
2345678901,1,Ted,Doe,John Doe,2007-03-17,male,none
2345678901,2,Amy,Doe,John Doe,2010-04-09,female,none
3345678901,3,Other,Person,Some Person,2003-03-12,male,none
3345678901,4,Other,Guy,Some Person,2003-03-12,male,none
```

Include this as `places.txt` in the `~/.librecheck/config/` directory:

```
Test place
Another place
```

Include this as `checked.txt` in the `~/.librecheck/config/` directory:

```
The following are the ID's in people.csv of people who are checked into the system at the moment:

```
(The trailing new line is helpful for readability)

You can use the `setup-dev.sh` script (on GNU/Linux) to do this automatically. 