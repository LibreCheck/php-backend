<?php
/*
* LibreCheck - free check-in software | PHP Backend
* Copyright (C) 2018, 2019  MasterOfTheTiger
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// This file provides all the people in the people.csv file

$configLocation = getenv('HOME') . '/.librecheck';

$peopleFile = fopen($configLocation . "/config/people.csv","r");
$phone = strval($_POST['phone']);

// Grab all people in an array
$people = [];
$index = 0;
while(! feof($peopleFile)){
    $people[$index] = fgetcsv($peopleFile);
    $index++;
}


// Grab people who are currently checked in
$checked = fopen($configLocation . "/config/checked.txt","r");
// Pull lines into an array
$lines = [];
while(! feof($checked)){
    array_push($lines, fgets($checked));
}

// Grab all information about all people who are checked in
$checkedPeople = [];
for ($i=1; $i < count($lines); $i++) {
    for ($in=1; $in < count($people); $in++) {
        // See if ID 
        if (strval($people[$in][1]) == implode('', explode("\n", strval($lines[$i])))) {
            array_push($checkedPeople, $people[$in]);
        } else {
            //array_push($checkedPeople, 'null');
        }
    }
}

$response = new stdClass();

$response->checked = $checkedPeople;
$response->all = $people;

echo json_encode($response);

?>