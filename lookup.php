<?php
/*
* LibreCheck - free check-in software | PHP Backend
* Copyright (C) 2018, 2019  MasterOfTheTiger
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

$configLocation = getenv('HOME') . '/.librecheck';

$people = fopen($configLocation . "/config/people.csv","r");
$phone = strval($_POST['phone']);

$result = new stdClass();

// Grab all people as arrays in an array
$arrays = [];
$index = 0;
while(! feof($people)){
    $arrays[$index] = fgetcsv($people);
    $index++;
}

// Grab all people who are associated with the groupid in $arrays
$groupchildren = [];
for ($i=1; $i < count($arrays); $i++) {
    if ($arrays[$i][0] == $phone) {
        array_push($groupchildren, $arrays[$i]);
    }
}

// Grab people who are currently checked in
$checked = fopen($configLocation . "/config/checked.txt","r");
// Pull lines into an array
$lines = [];
while(! feof($checked)){
    array_push($lines, fgets($checked));
}

// Grab all information about all people who are checked in
$checkedPeople = [];
for ($i=1; $i < count($lines); $i++) {
    for ($in=1; $in < count($arrays); $in++) {
        // See if ID 
        if (strval($arrays[$in][1]) == implode('', explode("\n", strval($lines[$i])))) {
            array_push($checkedPeople, $arrays[$in]);
        } else {
            //array_push($checkedPeople, 'null');
        }
    }
}

// Grab all people who are associated with the groupid in $checkedPeople
$groupChecked = [];
for ($i=0; $i < count($checkedPeople); $i++) {
    if ($checkedPeople[$i][0] == $phone) {
        array_push($groupChecked, $checkedPeople[$i]);
    }
}

fclose($people);
// array_push($result, $groupchildren);
// array_push($result, $checkedPeople);
$result->all = $groupchildren;
$result->checked = $groupChecked;
echo json_encode($result);
?>
