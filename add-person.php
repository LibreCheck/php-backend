<?php
/*
* LibreCheck - free check-in software | PHP Backend
* Copyright (C) 2018, 2019  MasterOfTheTiger
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

$configLocation = getenv('HOME') . '/.librecheck';

$response = new stdClass();

// Grab the data set from the frontend
$phone = strval($_POST['phone']);
$guardname = strval($_POST['guardname']);
$children = (array) json_decode($_POST['children']);

$response->children = $children;

$people = fopen($configLocation . "/config/people.csv","a+");

$persons = [];
$index = 0;
while(! feof($people)){
    $persons[$index] = fgetcsv($people);
    $index++;
}

$response->kids = [];

$toadd = [];
for ($i=0; $i < count($children); $i++) {
    // Assign ID number for each child
    $pos = -2;
    while (fgetc($people) != "\n") {
        fseek($people, $pos, SEEK_END);
        $pos = $pos - 1;
    }
    $lastperson = fgetcsv($people);
    $number = intval($lastperson[1]) + 1;
    $child = get_object_vars($children[$i]);
    $response->lastperson = $lastperson;
    // Put all pieces together to what is used in people.csv
    $toadd[$i] = "\n" . $phone . "," . $number . "," . $child['first'] . "," . $child['last'] . "," . $guardname . "," . $child['dob'] . "," . $child['gender'] . "," . $child['medical'];
    // Write to end of file
    fseek($people, 0, SEEK_END);
    fwrite($people, $toadd[$i]);
    $response->kids[$i] = $children[$i];
}

echo json_encode($response);
fclose($people);
?>
