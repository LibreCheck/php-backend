<?php
/*
* LibreCheck - free check-in software | PHP Backend
* Copyright (C) 2018, 2019  MasterOfTheTiger
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

$configLocation = getenv('HOME') . '/.librecheck';

header("Content-Type: application/json; charset=UTF-8");
date_default_timezone_set('America/Los_Angeles');

$date = date('Y-m-d');
$time = date('H:i:s');
$data = json_decode($_POST['people']);
$places = json_decode($_POST['places']);

// Open and retrieve people data
$peoplefile = fopen($configLocation . "/config/people.csv","r");

$people = [];
$index = 0;
while(! feof($peoplefile)){
    $people[$index] = fgetcsv($peoplefile);
    $index++;
}

// Grab only people with the IDs mentioned in the POST data
$selectedPeople = [];

for ($a=0; $a < count($data); $a++) { 
    for ($i=1; $i < count($people); $i++) {
        if ($people[$i][1] == $data[$a]) {
            array_push($selectedPeople, $people[$i]);
        }
    }
}

$text = '';

for ($i=0; $i < count($selectedPeople); $i++) {
    $text = $text . $selectedPeople[$i][2] . ' ' . $selectedPeople[$i][3] . ' (ID '. $selectedPeople[$i][1] . ', group ID ' . $selectedPeople[$i][0] . ')' . " checked into \"" . $places[$i] . "\" place at " . $time . ' on ' . $date .PHP_EOL;
}

// Write to checked.txt file
$checked = fopen($configLocation . "/config/checked.txt","a+");
for ($i=0; $i < count($data); $i++) { 
    fwrite($checked, $data[$i] . "\n");
}

// Write to log file

$log = fopen($configLocation . '/logs/'.$date.'.txt',"a");

fwrite($log, $text);
fclose($log);
?>
